
#ifndef INCLUDES_H_
#define INCLUDES_H_

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"
#include "timers.h"



#define EDF_TASK_PRIORITY_MINIMUM 1
#define EDF_TASK_PRIORITY_GENERATOR (configMAX_PRIORITIES - 2)
#define EDF_TASK_PRIORITY_SCHEDULER (configMAX_PRIORITIES - 1)
#define EDF_MAX_TASKS (EDF_TASK_PRIORITY_SCHEDULER - 1 - EDF_TASK_PRIORITY_MINIMUM)

#endif
