#ifndef EDF_SCHEDULER_H_
#define EDF_SCHEDULER_H_

#include "EDF_ActiveList.h"
#include "includes.h"

/** \brief Message types */
typedef enum EDF_Message_Type {
  // 1.1 Add the missing field
  EDF_Message_Create,
  EDF_Message_Delete,
} EDF_Message_Type;

/** \brief Message structure used in the queue */
typedef struct EDF_Message {
  // 1.2 Add the missing field
  EDF_Message_Type message_type;
  TaskHandle_t message_sender;
  void *message_data;
} EDF_Message;

/** \brief it initialises the active list and queue, it also creates the
 * priority handler task
 */
void EDF_PriorityHandler_Init();

/** \brief it receives messages from the queue, depending on the message type,
 * it adds/removes a new task from the active list \param pvParameters required
 * by all FreeRTOS task functions
 */
void EDF_PriorityHandler(void *pvParameters);
/** \brief it receives messages from the queue, depending on the message type,
 * it adds/removes a new task from the active list \param pvParameters required
 * by all FreeRTOS task functions
 */

/** \brief it creates a new task using FreeRTOS APIs, it also sends a new
 * create-message on the queue; this function is called from
 * "taskGenerator" in EDF_Wrapper.c \param create_task the information
 * of the created task
 */
uint32_t EDF_Task_Create(EDF_Task *create_task);

/** \brief it removes a task using FreeRTOS APIs, it also sends a new
 * delete-message on the queue; this function is called from "genericTask" in
 * EDF_Wrapper.c \param delete_task the handle of task to be deleted
 */
uint32_t EDF_Task_Delete(TaskHandle_t delete_task);

#endif
