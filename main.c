/*
 * FreeRTOS V202012.00
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */

/* Standard includes. */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "includes.h"

/* Local includes. */
#include "EDF_PriorityHandler.h"
#include "EDF_Wrapper.h"
#include "console.h"
/*-----------------------------------------------------------*/

// periods of tasks created using "EDF_CutePeriodicCreate"
static int periods[EDF_MAX_TASKS] = {0};
// size of "periods"
static int pSize = 0;
// execution time of tasks created using "EDF_CutePeriodicCreate"
static int execs[EDF_MAX_TASKS] = {0};
// size of "execs"
static int eSize = 0;

/** \brief wrapper to easily create periodic tasks
 * \param name name of the task
 * \param deadline deadline of the task
 * \param period period of the task
 * \param execTime execTime of the task
 */
static void EDF_CutePeriodicCreate(char *name, int deadline, int period,
                                   int execTime);

/** \brief Utility function to find GCD of 'a' and 'b'
 */
static int gcd(int a, int b);

/** \brief Returns LCM of array elements
 */
static long long int findlcm(int arr[], int n);

/** \brief  returns the CPU utilisation of the created tasks
 */
double findUsage();

/** \brief stops the scheduler after N ticks
 */
void stop(void *pvParameters);

int gcd(int a, int b) {
  if (b == 0)
    return a;
  return gcd(b, a % b);
}

long long int findlcm(int arr[], int n) {
  long long int ans = arr[0];
  for (int i = 1; i < n; i++)
    ans = (((arr[i] * ans)) / (gcd(arr[i], ans)));

  return ans;
}
double findUsage() {
  // return the CPU utilisation for the created tasks
  double sum = 0.f;
  for (int i = 0; i < pSize; ++i) {
    sum += (double)execs[i] / (double)periods[i];
  }
  return sum;
}

void EDF_CutePeriodicCreate(char *name, int deadline, int period,
                            int execTime) {
  // allocate memory for a new PeriodicTaskParam
  PeriodicTaskParam *param =
      (PeriodicTaskParam *)pvPortMalloc(sizeof(PeriodicTaskParam));
  // init the new PeriodicTaskParam with EDF_CutePeriodicCreate inputs
  param->name = name;
  param->_deadline = deadline;
  param->_period = period;
  param->_execTime = execTime;
  // create a new periodic task with the given parameters
  EDF_PeriodicTaskCreate(param);
  // update periods and execs variables
  periods[pSize++] = param->_period;
  execs[eSize++] = param->_execTime;
}
void stop(void *pvParameters) {

  // retrieve the stop time from pvParameters
  int *stopAt = (int *)pvParameters;
  console_print("End time: %d\n\n", *stopAt);
  TickType_t now = xTaskGetTickCount();
  // delay until stop time
  vTaskDelayUntil(&now, *stopAt);

  console_print("\nCurrent time = %u\n", (unsigned int)xTaskGetTickCount());
  // stop the scheduler
  vTaskEndScheduler();
}

int main(void) {
  console_init();

  // 12.1 initialise the priority handler
  EDF_PriorityHandler_Init();

  // create the tasks <name,deadline,period,exeTime>
  EDF_CutePeriodicCreate("Task1", 3000, 3000, 1000);
  EDF_CutePeriodicCreate("Task2", 6000, 6000, 500);
  EDF_CutePeriodicCreate("Task3", 9000, 9000, 3000);

  // 12.2 find the least common multiplier
  int lcm = findlcm(periods, pSize);
  // 12.3 print the CPU usage (in percentage)
  console_print("Usage: %d\%\n", (unsigned int)(findUsage() * 100.f));
  // 12.4 create the stopper task using the 'stop' function as a task function
  xTaskCreate(stop, "", configMINIMAL_STACK_SIZE, (void *)&lcm,
              EDF_TASK_PRIORITY_SCHEDULER, NULL);

  console_print(
      "---------------------- [Scheduler start] ---------------------------\n");
  vTaskStartScheduler();
  console_print(
      "---------------------- [Scheduler stop] ---------------------------\n");
  return 0;
}
/*-----------------------------------------------------------*/
