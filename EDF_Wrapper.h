
#ifndef EDF_TASK_CREATOR_H_
#define EDF_TASK_CREATOR_H_

#include "EDF_ActiveList.h"
#include "includes.h"

/** \brief it creates a task with the given parameters
 * \param param task parameters
 */
void EDF_PeriodicTaskCreate(PeriodicTaskParam *param);

#endif
