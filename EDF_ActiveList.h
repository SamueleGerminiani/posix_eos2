#ifndef EDF_TASK_LIST_H_
#define EDF_TASK_LIST_H_

#include "CustomQueue.h"
#include "includes.h"

/** \brief struct containing the periodic task parameters
 */
typedef struct PeriodicTaskParam {
  // 1.3 Add the missing field
  int _period;
  int _deadline;
  int _execTime;
  char *name;
} PeriodicTaskParam;

/** \brief struct containing the active task parameters
 */
typedef struct EDF_Task {
  // 1.4 Add the missing field
  TaskHandle_t task_handle;
  TaskFunction_t task_function;
  const char *task_name;
  TickType_t creation_time;
  TickType_t deadline;
  TickType_t executionTime;
  LIST_ENTRY(EDF_Task) pointers;
} EDF_Task;

/** \brief defining the active list type
 */
LIST_HEAD(EDF_TaskList_t, EDF_Task);

/** \brief it allocates memory and initialises a new EDF_Task
 */
EDF_Task *EDF_Task_Allocate();

/** \brief it frees the memory associated with a EDF_Task*
 * \param task_to_remove task to remove
 */
bool EDF_Task_Free(EDF_Task *task_to_remove);

/** \brief it initialises the active task list
 */
void EDF_TaskList_Init();

/** \brief it inserts a new task in the active list
 * \param task_to_insert task to insert
 */
void EDF_TaskList_Deadline_Insert(EDF_Task *task_to_insert);

/** \brief it removes a task in the active list
 * \param task_to_remove task to remove
 */
void EDF_TaskList_Remove(TaskHandle_t task_to_remove);

#endif
